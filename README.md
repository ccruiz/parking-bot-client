### Summary ###
HTML Client that will run as a SPA for the parking-bot project.

### Setting it up ###
Please use NVM to guarantee right version of node is used for compiling assets. Doubts about setting it up?. [Check NVM Repo](https://github.com/creationix/nvm)

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact